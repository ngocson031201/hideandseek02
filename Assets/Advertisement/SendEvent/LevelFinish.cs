using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SendEventAppMetrica
{
    public class LevelFinish
    {
        private const string nameEvent = "level_finish";
        public static EventAppmetrica eventAppmetrica;

        /// <summary>
        /// Thay đổi tất
        /// </summary>
        public static void SetEvent(int level_number, string level_name,   int    level_count, string level_diff, 
            int level_loop,   bool   level_random, string level_type,  string game_mode,string result, int time, int progress, int continueAds)
        {
            eventAppmetrica.parameters[LevelFinishParameters.level_number] = level_number;
            eventAppmetrica.parameters[LevelFinishParameters.level_name]   = level_name;
            eventAppmetrica.parameters[LevelFinishParameters.level_count]  = level_count;
            eventAppmetrica.parameters[LevelFinishParameters.level_diff]   = level_diff;
            eventAppmetrica.parameters[LevelFinishParameters.level_loop]   = level_loop;
            eventAppmetrica.parameters[LevelFinishParameters.level_random] = level_random;
            eventAppmetrica.parameters[LevelFinishParameters.level_type]   = level_type;
            eventAppmetrica.parameters[LevelFinishParameters.game_mode]    = game_mode;
            eventAppmetrica.parameters[LevelFinishParameters.result]    = result;
            eventAppmetrica.parameters[LevelFinishParameters.time]    = time;
            eventAppmetrica.parameters[LevelFinishParameters.progress]    = progress;
            eventAppmetrica.parameters[LevelFinishParameters.continueAds]    = continueAds;
        }

        /// <summary>
        /// Thay đổi đơn lẻ giá trị levelstart
        /// </summary>
        /// <param name="nameParameter">Get from LevelFinishParameters. Eg: LevelFinishParameters.level_number</param>
        /// <param name="value">eg: bool, int, string</param>
        public static void SetEvent(string nameParameter, object value)
        {
            eventAppmetrica.parameters[nameParameter] = value;
        }

        public static void InitDefault()
        {
            eventAppmetrica = new EventAppmetrica(nameEvent, new Dictionary<string, object>()
            {
                {LevelFinishParameters.level_number, -1},
                {LevelFinishParameters.level_name, "init"},
                {LevelFinishParameters.level_count, -1},
                {LevelFinishParameters.level_diff, "init"},
                {LevelFinishParameters.level_loop, -1},
                {LevelFinishParameters.level_random, false},
                {LevelFinishParameters.level_type, "init"},
                {LevelFinishParameters.game_mode, "init"},
                {LevelFinishParameters.result, "init"},
                {LevelFinishParameters.time, -1},
                {LevelFinishParameters.progress, -1},
                {LevelFinishParameters.continueAds, -1},
            });
        }
        
        public static void CustomParameters(Dictionary<string, object> parameter)
        {
            eventAppmetrica = new EventAppmetrica(nameEvent, parameter);
        }
        
        public static EventAppmetrica GetEventAppmetrica()
        {
            return eventAppmetrica;
        }
    }
    
    
    [System.Serializable]
    public static class LevelFinishParameters
    {
        public const string level_number = "level_number";
        public const string level_name   = "level_name";
        public const string level_count  = "level_count";
        public const string level_diff   = "level_diff";
        public const string level_loop   = "level_loop";
        public const string level_random = "level_random";
        public const string level_type   = "level_type";
        public const string game_mode = "game_mode";
        public const string result = "result";
        public const string time = "time";
        public const string progress = "progress";
        public const string continueAds = "continue";
    }
    
}
