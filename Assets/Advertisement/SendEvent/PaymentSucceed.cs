using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SendEventAppMetrica
{
    public class PaymentSucceed
    {
        private const string          nameEvent = "payment_succeed";
        public static EventAppmetrica eventAppmetrica;

        /// <summary>
        /// Thay đổi tất
        /// </summary>
        public static void SetEvent(string inapp_id,string currency, float price, string inapp_type)
        {
            eventAppmetrica.parameters[PaymentSucceedParameters.inapp_id] = inapp_id;
            eventAppmetrica.parameters[PaymentSucceedParameters.currency] = currency;
            eventAppmetrica.parameters[PaymentSucceedParameters.price] = price;
            eventAppmetrica.parameters[PaymentSucceedParameters.inapp_type] = inapp_type;
        }

        /// <summary>
        /// Thay đổi đơn lẻ giá trị levelstart
        /// </summary>
        /// <param name="nameParameter">Get from PaymentSucceedParameters. Eg: PaymentSucceedParameters.inapp_id</param>
        /// <param name="value">eg: bool, int, string</param>
        public static void SetEvent(string nameParameter, object value)
        {
            eventAppmetrica.parameters[nameParameter] = value;
        }

        public static void InitDefault()
        {
            eventAppmetrica = new EventAppmetrica(nameEvent, new Dictionary<string, object>()
            {
                {PaymentSucceedParameters.inapp_id, "init"},
                {PaymentSucceedParameters.currency, "init"},
                {PaymentSucceedParameters.price, -1f},
                {PaymentSucceedParameters.inapp_type, "init"},
            });
        }

        public static void CustomParameters(Dictionary<string, object> parameter)
        {
            eventAppmetrica = new EventAppmetrica(nameEvent, parameter);
        }

        public static EventAppmetrica GetEventAppmetrica()
        {
            return eventAppmetrica;
        }
    }
    
    [System.Serializable]
    public static class PaymentSucceedParameters
    {
        public const string inapp_id = "inapp_id";
        public const string currency = "currency";
        public const string price = "price";
        public const string inapp_type = "inapp_type";
    }

}
