using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PreConsts
{
    public const string COIN_N = "coin_n";
    public const string LEVEL_L = "level_l";
    public const string SKIN_N = "skin_n";
    public const string DAY_Y = "day_y";
    public const string TIME_TODAY = "time_today";
    public const string DAILY_FIRST = "daily_first";
    public const string DAILY_CLICK = "daily_click";
    public const string KEY_Y = "key_y";
    public const string HEALTHBAR_R = "Healthbar_r";
    public const string VOLUME_E = "Volume_e";
    public const string AVATAR_R = "Avatar_r";
    public const string CHECKUNLOCKALLSKIN_N = "CheckUnlockAllSkin_n";
    public const string CHECKSTARTERPACK_K = "CheckStarerPack_k";
    public const string BANNERADS_R = "Banner_r";
    public const string FULLADS_S = "Fullads_s";
    public const string REWARDADS_S = "Rewardads_s";
}
