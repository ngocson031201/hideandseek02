﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Shop : MonoBehaviour
{
    GameObject ItemTemplate;
    GameObject g;
    public Transform ShopScrollView;
    [SerializeField] GameObject HomeShopProfile;
    [SerializeField] GameObject ShopBuy;
    bool isWatchVideo;
    [System.Serializable]
    public class ShopItem
    {

        public int id;
        public Sprite Image;
        //public Sprite VideoObj;
        public int Price;
        public bool IsPurchased = false;
        public bool IsVideoBtn = false;
        public void SetData(int id)
        {
            this.id = id;

        }

    }    //lớp Item

    public List<ShopItem> ShopItemList;
    [SerializeField] GameObject PanelDialogEnoughCoin;
    public Sprite image;
    Button buyBnt;
    #region Singleton: Shop
    public static Shop instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        SetData();

    }
    #endregion


    private void Start()
    {
        ItemTemplate = ShopScrollView.GetChild(0).gameObject;
        int len = ShopItemList.Count;

        for (int i = 0; i < len; i++)
        {
            g = Instantiate(ItemTemplate, ShopScrollView);
            g.transform.GetChild(2).GetComponent<Image>().sprite = ShopItemList[i].Image;
            g.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = ShopItemList[i].Price.ToString();
            buyBnt = g.transform.GetChild(3).GetComponent<Button>();
            buyBnt.interactable = !ShopItemList[i].IsPurchased;
            if (ShopItemList[i].IsVideoBtn == true)//hiển thị nút xem video
            {
                g.transform.GetChild(3).GetComponent<Image>().sprite = image;
                g.transform.GetChild(3).GetChild(0).GetComponent<Text>().enabled = false;
                g.transform.GetChild(3).GetChild(1).GetComponent<Image>().enabled =false;
                buyBnt.AddEventListener(i, Reward);// xem video nhận thưởng 
            }
            else
            {
                buyBnt.AddEventListener(i, OnShopItemBtnClicked);//biết được khi người dùng mua item nào
            }    

            //if (ShopItemList[i].IsPurchased)
            //{
            //    buyBnt.interactable = false;
            //}
        }
        Destroy(ItemTemplate);
    }

    //đây là phương thức nhận thưởng khi xem video
    public void Reward(int itemindex)
    {
        ManagerAds.ins.ShowRewarded((x) =>
        {
            if (x)
            {

                //hiển thị là đã mua bằng dấu tích
                ShopScrollView.GetChild(itemindex).GetChild(4).gameObject.SetActive(true);

                //hiển thị background tối đằng sau nhân vật

                ShopScrollView.GetChild(itemindex).GetChild(0).gameObject.SetActive(true);

                //add avatar cho nó hiện thị

                ProfileShopUI.instance.AddAvatar(ShopItemList[itemindex].Image, ShopItemList[itemindex].id);
                GameManager.checkIsUserPlayGame++;

                GameManager.instance.SaveSkin(ShopItemList[itemindex - 1].id);
            }
        });
    }


    private void SetData()
    {
        for (int i = 0; i < ShopItemList.Count; i++)
        {
            ShopItemList[i].SetData(i + 1);
        }
    }



    void OnShopItemBtnClicked(int itemindex)
    {
        if (ScoreManager.instance.HasEnoughCoins(ShopItemList[itemindex].Price))
        {
            ScoreManager.instance.UseCoins(ShopItemList[itemindex].Price);
            //mua item
            ShopItemList[itemindex].IsPurchased = true;

            //hiển thị là đã mua bằng dấu tích
            ShopScrollView.GetChild(itemindex).GetChild(4).gameObject.SetActive(true);

            //hiển thị background tối đằng sau nhân vật

            ShopScrollView.GetChild(itemindex).GetChild(0).gameObject.SetActive(true);

            //add avatar cho nó hiện thị

            ProfileShopUI.instance.AddAvatar(ShopItemList[itemindex].Image, ShopItemList[itemindex].id);
            GameManager.checkIsUserPlayGame++;

            GameManager.instance.SaveSkin(ShopItemList[itemindex - 1].id);
        }
        else
        {
            PanelDialogEnoughCoin.SetActive(true);
        }
    }

    public void DisablePanelDialogEnoughCoin()
    {
        PanelDialogEnoughCoin.SetActive(false);
    }

    public void OpenShopBuy()
    {
        HomeShopProfile.SetActive(false);
        ShopBuy.SetActive(true);

    }

    public void CloseShopBuy()
    {
        HomeShopProfile.SetActive(true);
        ShopBuy.SetActive(false);
    }
}
