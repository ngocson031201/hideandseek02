using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Diamond : MonoBehaviour
{
    [SerializeField] float turnSpeed = 90f;

    void Update()
    {
        transform.Rotate(0, -turnSpeed * Time.deltaTime, 0);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Wall" || other.gameObject.tag == "Floor" || other.gameObject.tag == "Gold" || other.gameObject.tag == "Coin")
        {
            Destroy(gameObject);
            return;
        }

        if (other.gameObject.GetComponent<PlayerController>() != null)
        {

            Destroy(gameObject);
            ScoreManager.instance.AddPointDiamond();
            UIController.instance.CoinTextt(true);
            return;
        }
    }
}
